<?php

namespace app\controllers;

use app\models\Projects;
use app\models\Tasks;
use lithium\action\DispatchException;

class ProjectsController extends \lithium\action\Controller {

	public function index() {
		$projects = Projects::all();
		return compact('projects');
	}

	public function view() {
		$show_completed = isset($this->request->args[1]) && 'completed'===$this->request->args[1];
		// $project = Projects::find($this->request->args[0], array('with' => 'Tasks', 'order' => 'priority desc, Tasks.id asc'));
		$project = Projects::find($this->request->args[0]);
		$tasks = Tasks::all(array('conditions' => array('project_id' => $project->id, 'status' => (int)$show_completed), 'order' => 'priority desc'));
		// TODO show 404 if project not found
		if (!$project) {
			return $this->redirect('projects::index');
		}
		return compact('project', 'tasks', 'show_completed');
	}

	public function add() {
		$project = Projects::create();
		$project->created = date('Y-m-d H:i:s');
		$project->updated = date('Y-m-d H:i:s');

		if (($this->request->data) && $project->save($this->request->data)) {
			return $this->redirect(array('projects::view', 'args' => array($project->id)));
		}
		return compact('project');
	}

	public function edit() {
		$project = Projects::find($this->request->args[0]);
		$project->updated = date('Y-m-d H:i:s');

		if (!$project) {
			return $this->redirect('projects::index');
		}
		if (($this->request->data) && $project->save($this->request->data)) {
			return $this->redirect(array('projects::view', 'args' => array($project->id)));
		}
		return compact('project');
	}

	public function delete() {
		$project = Projects::find($this->request->args[0], array('with' => 'Tasks'));
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			return compact('project');
		}
		$project->tasks->delete();
		$project->delete();
		return $this->redirect('projects::index');
	}
}

?>