<?php

namespace app\controllers;

use app\models\Tasks;
use app\models\Projects;
use lithium\action\DispatchException;

class TasksController extends \lithium\action\Controller {

	public function index() {
		$show_completed = isset($this->request->args[0]) && 'completed'===$this->request->args[0];
		$tasks = Tasks::all(array('with' => 'Projects', 'conditions' => array('status' => (int)$show_completed), 'order' => 'priority desc, Tasks.id asc'));
		return compact('tasks', 'show_completed');
	}

	public function view() {
		$task = Tasks::find($this->request->args[0], array('with' => 'Projects'));
		// TODO show 404 if task not found
		if (!$task) {
			return $this->redirect('tasks::index');
		}
		return compact('task');
	}

	public function add() {
		$task = Tasks::create();
		$task->created = date('Y-m-d H:i:s');
		$task->updated = date('Y-m-d H:i:s');

		if (($this->request->data) && $task->save($this->request->data)) {
			if (isset($this->request->args[0])) {
				return $this->redirect(array('projects::view', 'args' => array($this->request->args[0])));
			} else {
				return $this->redirect(array('tasks::index'));
			}
		}

		if (isset($this->request->args[0])) {
			$current_project = $this->request->args[0];
		} else {
			$current_project = 0;
		}

        // Create a list of projects to send to the view.
        $projects = Projects::find('all');
        $projects_list = array();
        foreach($projects as $project) {
            $projects_list[$project->id] = $project->title;
        }
		return compact('task', 'projects_list', 'current_project');
	}

	public function edit() {
		$task = Tasks::find($this->request->args[0]);
		$task->updated = date('Y-m-d H:i:s');

		if (!$task) {
			return $this->redirect('tasks::index');
		}
		if (($this->request->data) && $task->save($this->request->data)) {
			return $this->redirect(array('tasks::view', 'args' => array($task->id)));
		}

        // Create a list of projects to send to the view.
        $projects = Projects::find('all');
        $projects_list = array();
        foreach($projects as $project) {
            $projects_list[$project->id] = $project->title;
        }
		return compact('task', 'projects_list');
	}

	public function delete() {
		$task = Tasks::find($this->request->args[0]);
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			return compact('task');
		}
		$task->delete();
		return $this->redirect('tasks::index');
	}

	public function mark_success() {
		if ($task = Tasks::find($this->request->args[0])) {
			$task->updated = date('Y-m-d H:i:s');
			$task->status = 1;	
			if ($task->save()) {
				return $this->redirect(array('tasks::view', 'args' => array($task->id)));
			}
		}
		return $this->redirect('tasks::index'); 
	}
}

?>
