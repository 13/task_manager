$(function(){
	$("table.table-tasks-projects").children("tbody").on("click", "tr", function(){
		window.location = $(this).find(".link").attr("href");
	});
	$("table.table-tasks-projects").find("a").on("click", function(event){
		event.stopImmediatePropagation();
	});
});
