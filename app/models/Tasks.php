<?php

namespace app\models;

class Tasks extends \lithium\data\Model {

	// used priorities and statuses:

	public static $priorities = array(
		0 => 'low',
		1 => 'normal',
		2 => 'high',
	);

	public static $statuses = array(
		0 => 'new',
		1 => 'done',
	);

	public $belongsTo = array('Projects');

	public $validates = array(
		'title' => array(
			array(
				'notEmpty',
				'message' => 'You must include title of task.',
			),
		),
		'project_id' => array(
			array(
				'notEmpty',
				'message' => 'You must include project of task.',
			),
		),
	);

}

?>
