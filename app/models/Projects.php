<?php

namespace app\models;

class Projects extends \lithium\data\Model {

	public $hasMany = array('Tasks');

	public $validates = array(
		'title' => array(
			array(
				'notEmpty',
				'message' => 'You must include title of task.',
			),
		),
	);
}

?>