<?php
	$this->title('Tasks');
?>

<h1>Tasks</h1>

<hr>

<?= $this->html->link('Add task', 'tasks::add', array('class' => 'btn btn-default')) ?>
<?php if ($show_completed) {
	echo $this->html->link('Show current tasks', array('tasks::index'), array('class' => 'btn btn-default'));
} else {
	echo $this->html->link('Show completed tasks', array('tasks::index', 'args' => array('completed')), array('class' => 'btn btn-default'));
} ?>

<table class="table table-hover table-tasks-projects">
	<thead>
		<tr>
			<th>id</th>
			<th>task</th>
			<th>description</th>
			<th>created</th>
			<th>project</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($tasks as $task) { ?>
			<tr<?php if (1===(int)$task->status) { ?> class="success"<?php } ?>>
				<td><?= $task->id ?></td>
				<td>
					<?= $this->html->link($task->title, array('tasks::view', 'args' => array($task->id)), array('class' => 'link')) ?>
					<?php if (2===(int)$task->priority) { ?>
						<span class="label label-primary" title="high priority">high</span>
					<?php } else if (0===(int)$task->priority) { ?>
						<span class="label label-default" title="low priority">low</span>
					<?php } ?>
				</td>
				<td class="description"><?= $task->description ?></td>
				<?php /*<td class="date"><?= date_format(date_create($task->created), 'd.m.Y') ?></td>*/ ?>
				<td class="date"><?= date_format(date_create($task->created), 'Y-m-d') ?></td>
				<td><?= $this->html->link($task->project, array('projects::view', 'args' => array($task->project->id))) ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
