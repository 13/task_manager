<?php
	$this->form->config(
		array(
			'templates' => array(
				'text' => '<div class="col-lg-11"><input class="form-control" type="text" name="{:name}"{:options} /></div>',
				'textarea' => '<div class="col-lg-11"><textarea class="form-control" rows="3" name="{:name}"{:options}>{:value}</textarea></div>',
				'select' => '<div class="col-lg-11"><select name="{:name}"{:options}>{:raw}</select></div>',
				'submit' => '<div class="form-group"><div class="col-lg-1"></div><div class="col-lg-11"><input type="submit" value="{:title}"{:options} /></div></div>',
				'error' => ' has-error',
				'field' => '<div class="form-group{:error}"{:wrap}>{:label}{:input}</div>',
			),
			'label' => array('class' => 'col-lg-1 control-label'),
		)
	);
	// $this->form->config();
?>

<h1>Add task</h1>

<hr>

<?= $this->form->create($task, array('class'=>'form-horizontal', 'role'=>'form')) ?>
	<?= $this->form->field('title', array('label'=>'Task')) ?>
	<?= $this->form->field('description', array('type' => 'textarea')) ?>
	<?= $this->form->field('project_id', array('list' => $projects_list, 'value' => $current_project, 'label'=>'Project', 'class'=>'form-control')) ?>			
	<?= $this->form->field('priority', array('list' => app\models\Tasks::$priorities, 'class'=>'form-control')) ?>
	<?= $this->form->submit('Save', array('class'=>'btn btn-default')) ?>
<?= $this->form->end() ?>
