<h1><?= $task->title ?></h1>

<hr>

<?//= $this->html->link('Add task', 'tasks::add', array('class' => 'btn btn-default')) ?>
<?php
	if (!$task->status) {
		echo $this->html->link('Mark as completed', array('tasks::mark_success', 'args' => array($task->id)), array('class' => 'btn btn-default'));
	}
?>
<?= $this->html->link('Edit', array('tasks::edit', 'args' => array($task->id)), array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Delete', array('tasks::delete', 'args' => array($task->id)), array('class' => 'btn btn-default')) ?>

<hr>

<?= $task->description ?>

<br>
<br>

<p><span class="label label-default">id #<?= $task->id ?></span></p>
<p><span class="label label-default">Created: <?= $task->created ?></span></p>
<p><span class="label label-default">Updated: <?= $task->updated ?></span></p>
<p>Priority: <?= app\models\Tasks::$priorities[$task->priority] ?></p>
<p>Status: <?= app\models\Tasks::$statuses[$task->status] ?></p>
<p>Project: <?= $this->html->link($task->project, array('projects::view', 'args' => array($task->project->id))) ?></p>
