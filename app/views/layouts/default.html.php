<?php
	$navigation = array(
		'Tasks' => 'tasks::index',
		'Projects' => 'projects::index',
	);
?>
<!doctype html>
<html>
<head>
	<?= $this->html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $this->title() ?></title>
	<?= $this->html->style(array('bootstrap.min', 'styles')) ?>
	<?= $this->html->script(array('//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js', 'bootstrap.min', 'app')) ?>
	<?= $this->scripts() ?>
	<?= $this->styles() ?>
</head>
<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<?php foreach ($navigation as $anchor => $link) { ?>
						<li<?php if (lithium\net\http\Router::match($link)===$this->request()->url) { ?> class="active"<?php } ?>>
							<?= $this->html->link($anchor, $link) ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<?= $this->content() ?>
	</div>
</body>
</html>