<?php
	$this->title('Projects');
?>

<h1>Projects</h1>

<hr>

<?= $this->html->link('Add project', 'projects::add', array('class' => 'btn btn-default')) ?>

<table class="table table-hover table-tasks-projects">
	<thead>
		<tr>
			<th>id</th>
			<th>created</th>
			<th>title</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($projects as $project) { ?>
			<tr>
				<td><?= $project->id ?></td>
				<td><?= date_format(date_create($project->created), 'd.m.Y') ?></td>
				<td><?= $this->html->link($project->title, array('projects::view', 'args'=>array($project->id)), array('class' => 'link')) ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
