<?= $this->form->create() ?>
	<p>Do you really want do delete project #<?= $project->id ?> "<?= $project->title ?>"?</p>
	<?php if ($project->tasks->to('array')) { ?>
		<p>It will also delete all related tasks:</p>
		<ul class="list-group">
			<?php foreach ($project->tasks as $task_key => $task) { ?>
				<li class="list-group-item">#<?= $task_key ?> - <?= $task ?></li>
			<?php } ?>
		</ul>
	<?php } ?>
	<?= $this->form->submit('Delete', array('class'=>'btn btn-default')) ?>
<?= $this->form->end() ?>
