<?php
	$this->form->config(
		array(
			'templates' => array(
				'text' => '<div class="col-lg-11"><input class="form-control" type="text" name="{:name}"{:options} /></div>',
				'textarea' => '<div class="col-lg-11"><textarea class="form-control" rows="3" name="{:name}"{:options}>{:value}</textarea></div>',
				'submit' => '<div class="form-group"><div class="col-lg-1"></div><div class="col-lg-11"><input type="submit" value="{:title}"{:options} /></div></div>',
				'error' => ' has-error',
				'field' => '<div class="form-group{:error}"{:wrap}>{:label}{:input}</div>',
			),
			'label' => array('class' => 'col-lg-1 control-label'),
		)
	);
?>

<h1>Edit project</h1>

<hr>

<?= $this->form->create($project, array('class'=>'form-horizontal', 'role'=>'form')) ?>
	<?= $this->form->field('title') ?>
	<?= $this->form->field('description', array('type' => 'textarea')) ?>
	<?= $this->form->submit('Save', array('class'=>'btn btn-default')) ?>
<?= $this->form->end() ?>
