<h1><?= $project->title ?></h1>

<hr>

<?= $this->html->link('Add project', 'projects::add', array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Edit', array('projects::edit', 'args' => array($project->id)), array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Delete', array('projects::delete', 'args' => array($project->id)), array('class' => 'btn btn-default')) ?>

<?php /*
<?= $this->html->link('Add project', 'projects::add', array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Edit', array('projects::edit', 'args' => array($project->id)), array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Delete', array('projects::delete', 'args' => array($project->id)), array('class' => 'btn btn-default')) ?>

<?= $this->html->link('Add task', 'tasks::add', array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Edit', array('tasks::edit', 'args' => array($task->id)), array('class' => 'btn btn-default')) ?>
<?= $this->html->link('Delete', array('tasks::delete', 'args' => array($task->id)), array('class' => 'btn btn-default')) ?>

*/ ?>

<hr>

<?php echo $project->description; ?>

<br>
<br>

<p><span class="label label-default">id #<?= $project->id ?></span></p>
<p><span class="label label-default">Created: <?= $project->created ?></span></p>
<p><span class="label label-default">Updated: <?= $project->updated ?></span></p>

<hr>

<?= $this->html->link('Add task to this project', array('tasks::add', 'args' => array($project->id)), array('class' => 'btn btn-default')) ?>
<?php if ($show_completed) {
	echo $this->html->link('Show current tasks', array('projects::view', 'args' => array($project->id)), array('class' => 'btn btn-default'));
} else {
	echo $this->html->link('Show completed tasks', array('projects::view', 'args' => array($project->id, 'completed')), array('class' => 'btn btn-default'));
} ?>

<table class="table table-hover table-tasks-projects">
	<thead>
		<tr>
			<th>id</th>
			<th>task</th>
			<th>description</th>
			<th>created</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($tasks as $task) { ?>
			<tr<?php if (1===(int)$task->status) { ?> class="success"<?php } ?>>
				<td><?= $task->id ?></td>
				<td>
					<?= $this->html->link($task->title, array('tasks::view', 'args' => array($task->id)), array('class' => 'link')) ?>
					<?php if (2===(int)$task->priority) { ?>
						<span class="label label-primary" title="high priority">high</span>
					<?php } else if (0===(int)$task->priority) { ?>
						<span class="label label-default" title="low priority">low</span>
					<?php } ?>
				</td>
				<td class="description"><?= $task->description ?></td>
				<td class="date"><?= date_format(date_create($task->created), 'd.m.Y') ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
